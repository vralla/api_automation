import pytest
from ..lib.BaseAPIClass import BaseAPIClass
from ..utils.helper import *

class APIFeed(BaseAPIClass):
    AUTH = None
    API_map = None
    base_url = None

    def get_APIMap(self):
        global API_map
        if not self.API_map:
            self.API_map = read_query_map('feed')
        return self.API_map

    def get_token(self):
        global AUTH
        if not self.AUTH:
            self.AUTH = self.get_auth()
        return self.AUTH

    def get_endpoint(self):
        endpoint = None
        try:
            endpoint = self.get_APIMap()['end_point']
        except:
            print('end_point is missing in the API map')
        return endpoint

    def get_url(self):
        base_url = self.get_base_url()
        endpoint = self.get_endpoint()
        url = base_url + endpoint
        return url

    def get_query_response(self, method, **kwargs):
        api_key = self.get_token()
        url = self.get_url()
        result = self.query_api(method=method, url=url, auth=api_key, params=kwargs['params'])
        return result

    def validate_schema(self, data):
        global API_map
        result = self.check_schema_match(self.API_map['schema1'], data)
        return result

    '''
    this is mock data. 
    do not have access to real DB.
    '''
    @staticmethod
    def get_db_element_count(start_date, end_date):
        #Insert code to connect to DB
        # and run sql query to get element_count
        return 10

    def __repr__(self):
        return 'APIFeed'
