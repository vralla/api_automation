import pytest
import sys
import os
import json
import yaml

dirpath = os.getcwd()
datafiledir = dirpath + '/data/'
queryfiledir = dirpath + '/queryMap/'


def read_query_data(query):
    filename = datafiledir + query + '.yaml'
    return read_yaml(filename)


def read_query_map(query):
    filename = queryfiledir + query + '.json'
    return read_json(filename)


def read_json(file):
    try:
        with open(file, 'r') as fp:
            try:
                data = json.load(fp)
            except:
                print(repr(data))
    except FileNotFoundError as error:
        print(error)
    return data


def read_yaml(file):
    data = {}
    try:
        with open(file, 'r') as fp:
            try:
                data = yaml.load(fp)
            except yaml.YAMLError as err:
                print(err)
    except FileNotFoundError as error:
        print(error)
    return data

