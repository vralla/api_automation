import pytest


@pytest.mark.usefixtures('environment_init')
class BaseClass(object):

    def get_cert(self):
        pass

    def get_host(self):
        pass