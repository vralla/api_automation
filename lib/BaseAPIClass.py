import pytest
import requests
import requests_toolbelt
from requests.exceptions import HTTPError
import shutil
import json
from .BaseClass import BaseClass
import jsonschema
from jsonschema import validate

import logging
logging.basicConfig(level=logging.INFO)


class BaseAPIClass(BaseClass):
    base_url = None

    def get_auth(self):
        return self.env['API_TOKEN']

    def get_base_url(self):
        global base_url
        if not self.base_url:
            self.base_url = self.env['API_BASE_URL']
        return self.base_url

    def query_api(self, method, url, auth, **kwargs):
        # Add api_token
        query_methods = dict(GET=self._GET,
                             POST=self._POST,
                             PUT=self._PUT,
                             DELETE=self._DELETE,
                             HEAD=self._HEAD,
                             PATCH=self._PATCH,
                             )

        return query_methods[method](url=url, auth=auth, params=kwargs['params'])

    @staticmethod
    def check_schema_match(schema, data):
        logging.info(f'Validating the input data using json schema')
        try:
            validate(instance=data, schema=schema)
            logging.info(f'OK: Schema matched for input data value')
        except jsonschema.exceptions.ValidationError as ex:
            logging.info(f'ERROR: Schema did NOT match for data value: {item_no} with reason:'
                         f'{ex}')
            return False
        return True


    def _GET(self, url, auth, **kwargs):
        kwargs['params']['api_key'] = auth
        response = requests.get(url, params=kwargs['params'])
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            logging.info(f'HTTP error occurred: {http_err}')
        except Exception as err:
            logging.info(f'Other error occurred: {err}')
        else:
            logging.info(f'Query ran successfully!')
        return response


    def _POST(self, url, **kwargs):
        #requests.post(url, data=kwargs)
        pass

    def _PUT(self):
        pass

    def _DELETE(self):
        pass

    def _HEAD(self):
        pass

    def _PATCH(self):
        pass

    def run_api(self, method, end_point, data='', headers=''):
        url = self.env["API_Base_Url"]+end_point
        logging.info(f'Request:{method} url: {url}. data: {data}. headers: {headers}')
        response = requests.request(method=method,
                                            url=url,
                                            data=data,
                                            headers=headers)
        try:
            response.raise_for_status()
        except HTTPError as http_err:
            logging.info(f'HTTP error occurred: {http_err}')
        except Exception as err:
            logging.info(f'Other error occurred: {err}')
        else:
            logging.info(f'Query ran successfully!')
        return response.json()
