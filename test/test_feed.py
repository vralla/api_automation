import pytest
import sys; sys.path.append('../')
from ..APIObject.APIFeed import *
from ..utils.helper import *

import logging
logging.basicConfig(level=logging.INFO)


class SetupClass:
    test_data = None

    @pytest.fixture()
    def logging(self):
        logging.info(f'Setting up tests')
        logging.info(f'Running Tests....')
        yield logging
        print("\nCleaning Test Setup...")

    @pytest.fixture()
    def testdata(self):
        global test_data
        if not self.test_data:
            self.test_data = read_query_data('feed')
        return self.test_data

# @pytest.mark.incremental
class APIFeedTest(APIFeed, SetupClass):
    @classmethod
    def setup_class(cls):
        pass

    '''
    Test GET query is successful with response code 200
    
    '''
    @pytest.mark.apiresponsetest
    def test_api_response_GET(self, logging, testdata):
        for data in testdata:
            result = self.get_query_response(method='GET', params=testdata[data])
            assert result.status_code == 200


    '''
    Test GET query is successful with response code 200
    and the data returned matches the database
    '''
    @pytest.mark.dataintegritytest
    def test_data_integrity_GET(self, testdata, logging):
        print(testdata)
        for data in testdata:
            print(data)
            db_data = self.get_db_element_count(start_date=testdata[data]['start_date'],
                                                end_date=testdata[data]['end_date'])
            result = self.get_query_response(method='GET', params=testdata[data])
            api_data = result.json()
            assert db_data == api_data["element_count"]

    '''
    Test GET query is successful with response code 200
    and the schema returned matches what is defined in APIMap 
    '''

    @pytest.mark.schematest
    def test_api_schema(self, testdata, logging):
        # fetching data for first data set only
        for data in testdata:
            data = self.get_query_response(method='GET', params=testdata[data])
            result = self.validate_schema(data.json())
            assert result

    '''
    Test response time for GET query
    print(response.elapsed.total_seconds())
    fails if response time is > 0.5 [set appropriate time ]
    Performance under load is not part of this test. 
    '''
    @pytest.mark.performancetest
    def test_api_GET_responsetime(self, testdata, logging):
        for data in testdata:
            response = self.get_query_response(method='GET', params=testdata[data])
            response_time = response.elapsed.total_seconds()
            logging.info(f'GET response time for {testdata[data]}: {response_time}')
            assert response_time < 0.5


    '''
    Test required params are honored 
    and GET query is successful with response code 200
    '''
    @pytest.mark.requiredfieldstest
    def test_required_params_GET(self):
        pass


    '''
    parameters 'out of boundary' tests 
    '''
    @pytest.mark.negativetest
    def test_api_GET_OOB(self, testdata, logging):
        pass

    '''
    parameters invalid type tests 
    '''
    @pytest.mark.negativetest
    def test_api_GET_paramsinvalidtype(self, testdata, logging):
        pass

    @classmethod
    def teardown_class(cls):
        pass
