API Automation
========
This README is a high-level overview of API Automation project. Development process, architectural overview, deployment environment details/architecture, CI & CD details, production optimization details (TLS, load-balancing, CDN) &c.

This repo also contains a `docker-compose` file for python environment to run the tests on without users having to setup their own environment


## Important notes
### points of interest
point of interest                    | details
-------------------------------------|--------
Application/Webservice               | https://api.nasa.gov 
Automation framework                 | pytest 
test                                 | directory where tests reside
data                                 | directory where test data resides
lib                                  | directory where library classes and methods reside
util                                 | directory where utility classes and methods reside
queryMap                             | directory where metadata about APIs reside
APIObject                            | directory where API specific metadata reside


### docker
Ensure you have `docker-compose >= 1.11` installed as well. Use `brew` on Mac: `brew install docker-compose`; or on any platform with python & pip installed, use `pip instal docker-compose`. 

### local development
```bash
# Always start with a git pull on the master branch.
git pull

# Kill ALL containers (uneeded the first time around). You can be more selective if needed.
docker ps -qa | xargs docker rm -fv

# Kill ALL old data volumes. You can be more selective if needed here as well.
docker volume ls -q | xargs docker volume rm -f

# Pull latest images (happens automatically the first time).
docker-compose pull

# Bring up all services for CAWS3.
docker-compose up -d

# Tail service logs if needed.
docker-compose logs -f
```
