import pytest
import json
import configparser
import os

def pytest_addoption(parser):
    parser.addoption("--env", action="store", dest="environment", default="QA", type=str,
                     help="Environment you want to run the test against. \nExample: QA | PROD | SCALE")


@pytest.fixture(scope="session")
def cmdopt(request):
    cmd_options = {
        'env': request.config.getoption("--env")
    }
    return cmd_options


@pytest.fixture(scope="session")
def environment_init(request, cmdopt):
    print("*******")
    print(os.getcwd())
    os.getcwd().split('test')
    env_option = cmdopt['env'].upper()
    valid_envs = ['QA',
                  'PROD',
                  'SCALE',
                  ]
    if env_option not in valid_envs:
        msg_ptn = 'Invalid environment provided for --env option {}. Valid values: "{}". Exiting...'
        pytest.exit(msg_ptn.format(env_option, valid_envs))

    cfg = configparser.ConfigParser()
    cfg.read('./config.ini')
    env = cfg['QA']
    session = request.node
    for item in session.items:
        cls = item.getparent(pytest.Class)
        setattr(cls.obj, "env", env)


@pytest.fixture(scope="session")
def driver_init(request):
    from selenium import webdriver
    browser = 'Chrome'
    # if browser == 'Chrome':
    #     from seleniumrequests import Chrome
    #     web_driver = Chrome()
    #     request.cls.driver = web_driver

    web_driver = webdriver.Chrome()
    session = request.node
    for item in session.items:
        cls = item.getparent(pytest.Class)
        setattr(cls.obj, "driver", web_driver)

    yield
    web_driver.close()

    #
    # # yield approach should be applied prior to the finalizer
    # try:
    #     pass
    #     # request.addfinalizer(web_driver.close)
    # except:
    #     pass





def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._previousfailed = item


def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        previousfailed = getattr(item.parent, "_previousfailed", None)
        if previousfailed is not None:
            pytest.xfail("previous test failed (%s)" %previousfailed.name)

