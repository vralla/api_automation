FROM ubuntu:18.10

LABEL maintainer="vasudharalla@gmail.com"

# Port 8082 is the default port for the centralized server.
EXPOSE 8082

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qqy
RUN apt-get upgrade -qqy
RUN apt-get install -y apt-utils
RUN apt-get install -y python3.6
RUN apt-get install -y python-dev
RUN apt-get install -y python-pip
RUN apt-get install -y python3-pip
RUN apt-get install -y vim
RUN apt-get install git-all -y
RUN apt-get install wget -y
RUN apt-get install unzip -y
RUN apt-get install iputils-ping -y
RUN apt-get install xvfb -y
#RUN apt-get install -y postgresql postgresql-contrib
RUN apt-get install -y freetds-dev
RUN apt-get install -y freetds-bin
RUN echo "#define DBVERSION_80 DBVERSION_71" >> /usr/include/sybdb.h
RUN export PYMSSQL_BUILD_WITH_BUNDLED_FREETDS=1
#RUN export CFLAGS="-fPIC"
RUN pip3 install Cython
RUN pip3 install pymssql


RUN pip3 install --upgrade pip
RUN pip3 install pytest==3.6.4
#Appium libs
RUN pip3 install appium-python-client
RUN pip3 install ConfigParser
#Pytest utils/adds
RUN pip3 install pytest-html
RUN pip3 install pytest-cov
RUN pip3 install pytest-capturelog
RUN pip3 install pytest-xdist
RUN pip3 install pytest-django
RUN pip3 install pyPdf
RUN pip3 install pyyaml
RUN pip3 install requests
RUN pip3 install requests_toolbelt
RUN pip3 install graphql-core
RUN pip3 install selenium
RUN pip3 install enum34
RUN pip3 install psycopg2
RUN pip3 install pyvirtualdisplay
#Baitnet
RUN pip3 install pymssql
RUN pip3 install pyvmomi
RUN pip3 install aenum
RUN pip3 install flake8
#RUN pip3 install pika==0.11.0
RUN pip3 install slacker

RUN pip3 install python-geoip-geolite2
RUN pip3 install python-geoip
#RUN pip3 install peewee
#RUN pip3 install pyodbc
#RUN pip3 install peewee_mssql

#git+https://github.com/pymssql/pymssql

#RUN pip3 install pysftp
#RUN pip3 install dpkt
#RUN pip3 install pika

#Async py pkgs
#RUN pip3 install  asyncio
#RUN pip3 install  gevent

#=========
# Firefox
#=========
ARG FIREFOX_VERSION=54.0.1
RUN apt-get -qqy --no-install-recommends install firefox
RUN rm -rf /var/lib/apt/lists/* /var/cache/apt/*
RUN wget --no-verbose -O /tmp/firefox.tar.bz2 https://download-installer.cdn.mozilla.net/pub/firefox/releases/$FIREFOX_VERSION/linux-x86_64/en-US/firefox-$FIREFOX_VERSION.tar.bz2
RUN apt-get -y purge firefox
RUN rm -rf /opt/firefox
RUN tar -C /opt -xjf /tmp/firefox.tar.bz2
RUN rm /tmp/firefox.tar.bz2
RUN mv /opt/firefox /opt/firefox-$FIREFOX_VERSION
RUN ln -fs /opt/firefox-$FIREFOX_VERSION/firefox /usr/bin/firefox

#============
# GeckoDriver
#============
ARG GECKODRIVER_VERSION=0.18.0
RUN wget --no-verbose -O /tmp/geckodriver.tar.gz https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz
RUN rm -r -f /opt/geckodriver
RUN tar -C /opt -zxf /tmp/geckodriver.tar.gz
RUN rm /tmp/geckodriver.tar.gz
RUN mv /opt/geckodriver /opt/geckodriver-$GECKODRIVER_VERSION
RUN chmod 755 /opt/geckodriver-$GECKODRIVER_VERSION
RUN ln -fs /opt/geckodriver-$GECKODRIVER_VERSION /usr/bin/geckodriver

#============================================
# Google Chrome
#============================================
ARG CHROME_VERSION="google-chrome-stable"
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
RUN apt-get update -qqy
RUN apt-get -qqy install \
    ${CHROME_VERSION:-google-chrome-stable}
RUN rm /etc/apt/sources.list.d/google-chrome.list
RUN rm -rf /var/lib/apt/lists/* /var/cache/apt/*

#==================
# Chrome webdriver
#==================
ARG CHROME_DRIVER_VERSION=2.31
RUN wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip \
  && rm -rf /opt/selenium/chromedriver \
  && unzip /tmp/chromedriver_linux64.zip -d /opt/selenium \
  && rm /tmp/chromedriver_linux64.zip \
  && mv /opt/selenium/chromedriver /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && chmod 755 /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION \
  && ln -fs /opt/selenium/chromedriver-$CHROME_DRIVER_VERSION /usr/bin/chromedriver
